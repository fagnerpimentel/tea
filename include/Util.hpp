#ifndef UTIL_H
#define UTIL_H

#include <fstream>
#include "Matriz.hpp"
#include "csv.h"


Matriz readFile(char *filename){

    /** Leitura dos dados do arquivo **/

    // Declara vetores onde serão armazenados os dados
    std::vector< std::vector<double> > vector_line;
    std::vector<double> vector_element;

    // variaveis de leitura
    string line; // armazena cada linha do arquivo
    string element; // armazena cada elemento das linhas

    // Abre aquivo
    ifstream file (filename);
    if (file.is_open()) {

        // Varre aquivo
        while (!file.eof()) {
            getline (file,line);
            istringstream iss(line);

            // Varre linhas
            vector_element.clear();
            while(iss >> element) {

                // Armazena cada elemento das linhas em um vetor
                vector_element.push_back(atof(element.c_str()));

            }

            // Armazena cada linha em um vetor
            vector_line.push_back(vector_element);
        }

        // Fecha arquivo
        file.close();

    } else {
        cout << "Problemas ao abrir o aquivo!" << endl;
        exit(1);
    }
    cout << endl;


    /** Dados do arquivo para matriz **/

    Matriz dados = Matriz(vector_line.size() - 2, vector_line.at(0).size());
    for (unsigned int i = 1; i < vector_line.size(); i++) {
        vector_element = vector_line.at(i);
        for (unsigned int j = 0 ; j < vector_element.size(); j++) {
            dados.set(i-1, j, vector_element.at(j));
        }
    }
    return dados;
}


const vector<string> explode(const string& s, const char& c)
{
    string buff{""};
    vector<string> v;

    for(auto n:s)
    {
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);

    return v;
}

vector<vector<string>> readCSV(char *filename){
    io::LineReader in(filename);
    vector<vector<string>> dados;
    while(char*line = in.next_line()){
        string str{line};
        vector<string> v{explode(str, ',')};
        dados.push_back(v);
    }

    return dados;
}

#endif // UTIL_H
