#ifndef RNA_H
#define RNA_H

#include "Matriz.hpp"
#include "mersenne_twister.h"

class RNA
{
private:
    int n; // número de entradas
    int m; // número de neurônios na camada de saída
    int h; // número de neurônios em cada camada escondida
    int H; // número de camadas escondidas

    double e;   // erro mínimo
    double eta; // taxa de aprendizado

    int max_it; // máximo de iterações

    vector<Matriz> wh;// = Matriz(0,0); // matriz de pesos: camadas escondida
    vector<Matriz> whb;// = Matriz(0,0); // matriz de pesos: bias das camadas escondidas

    Matriz ws = Matriz(0,0); // matriz de pesos: camada de saída
    Matriz wsb = Matriz(0,0); // matriz de pesos: bias da camada de saída

public:
    // Responsável por inicializar a rna com seus principais parâmetros
    RNA(int n, int m, int h, int H, int max_it, double e, double eta);

    // Imprime informações sobre a rede
    string info();

    // Função de treinamento
    void train(Matriz entrada);
    // Função de teste
    double test(Matriz entrada);

private:
    // Função de ativação dos neurônios
    double f_net(double value);
    // Derivada da função de ativação dos neurônios
    double f_linha_net(double f);

};

#endif // RNA_H
