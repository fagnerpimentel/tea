#ifndef MACHINELEARNING_H
#define MACHINELEARNING_H

#include "Matriz.hpp"

// usado para organizar os auto vetores
typedef pair<double,int> eigenPair;
static bool comparator ( const eigenPair& l, const eigenPair& r){
    return l.first > r.first;
}

//// Classe MachineLearning
//class MachineLearning
//{
//private:
//    Matriz dados;
//public:
//    MachineLearning(Matriz dados_){
//        this->dados = dados;
//    }
//};

// organiza os auto vetores e auto valores
void organizaAutoValoresEAutoVetores(Matriz &autoValores, Matriz &autoVetores){

    Matriz aux1 = autoValores;
    Matriz aux2 = autoVetores;

    vector<eigenPair> epVector;
    for (int o = 0; o < autoValores.ordem(); ++o) {
        eigenPair ep = eigenPair(autoValores.get(o,o),o);
        epVector.push_back(ep);
    }
    sort(epVector.begin(), epVector.end(), comparator);

    for (int linha = 0; linha < autoValores.linhas(); linha++) {
        for (int coluna = 0; coluna < autoValores.colunas(); coluna++) {
            autoValores.set(linha, coluna, aux1.get(epVector.at(linha).second, epVector.at(coluna).second));
            autoVetores.set(linha, coluna, aux2.get(linha, epVector.at(coluna).second));
        }
    }

}

// determina quantidade de autovetores necessários para alcaçar uma dada variância mínima
int qtdeComponentes(Matriz autoValores, Matriz autoVetores, double varianciaMinima){
    // seleciona número de vetores
    int numeroDeVetores = 0;
    int ordem = autoValores.ordem();
    double somaDasVariancias = 0;
    for (int i = 0; i < ordem; i++) {
        double varianciaDoVetor = 0;
        for (int j = 0; j < ordem; j++) {
            varianciaDoVetor += autoValores.get(j,j);
        }
        double r = autoValores.get(i,i)/varianciaDoVetor;
        cout << "O vetor " << (i+1) << " engloba " << r*100 << " % da variância" << endl;
        somaDasVariancias += r;
        if(somaDasVariancias < varianciaMinima) {
            numeroDeVetores++;
        }

    }

    return numeroDeVetores + 1;
}

Matriz PCA(Matriz dados, double varianciaMinima){

    /** Média dos dados **/
    Matriz media = dados.media();
    cout << "Média dos dados:" << endl;
    cout << media.toString() << endl;

    /** Normalização dos dados **/
    cout << "Normalização dos dados:" << endl;
    Matriz media_expandida = Matriz(dados.linhas(), dados.colunas());
    for (int linha = 0; linha < dados.linhas(); linha++) {
        for (int coluna = 0; coluna < dados.colunas(); coluna++) {
           media_expandida.set(linha, coluna, media.get(0, coluna));
        }
    }
    Matriz normalizada = dados - media_expandida;
    cout << normalizada.toString() << endl;

    /** Matriz de covariância **/
    cout << "Matriz de covariância:" << endl;
    Matriz covariancia = normalizada.covariancia();
    cout << covariancia.toString() << endl;

    /** Calculo dos auto valores e auto vetores **/
    Matriz autoValores = Matriz(covariancia.ordem(), covariancia.ordem());
    Matriz autoVetores = Matriz(covariancia.ordem(), covariancia.ordem());
    covariancia.eigen(1000, 0.05, autoValores, autoVetores);
    organizaAutoValoresEAutoVetores(autoValores, autoVetores);
    cout << "Auto valores:" << endl;
    cout << autoValores.toString() << endl;
    cout << "Auto vetores:" << endl;
    cout << autoVetores.toString() << endl;

    /** Determina quantidade de componentes **/
//    double varianciaMinima = 0.98;
    int qtde = qtdeComponentes(autoValores, autoVetores, varianciaMinima);
    cout << qtde << " vetor(es) representa(m) pelo menos " << varianciaMinima*100 << "% da variância" << endl << endl;

    /** Vetor de características **/
    cout << "Vetores de características:" << endl;
    Matriz vetorDeCaracteristicas = Matriz(autoVetores.linhas(), qtde);
    for (int linha = 0; linha < autoVetores.linhas(); linha++) {
        for (int coluna = 0; coluna < qtde; coluna++) {
            vetorDeCaracteristicas.set(linha, coluna, autoVetores.get(linha, coluna));
        }
    }
    cout << vetorDeCaracteristicas.toString() << endl;

    /** Novo dataset **/
    cout << "Novo dataset:" << endl;
    Matriz dadosFinais = vetorDeCaracteristicas.transposta() * normalizada.transposta();
    Matriz novoDataset = (vetorDeCaracteristicas * dadosFinais) + media_expandida.transposta();
    cout << novoDataset.transposta().toString() << endl;

    return novoDataset.transposta();
}

#endif // MACHINELEARNING_H
