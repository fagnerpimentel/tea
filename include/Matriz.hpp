#ifndef MATRIZ_H
#define MATRIZ_H

#include <algorithm>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <stdexcept>

using namespace std;

// Classe Matriz
class Matriz
{
private:

    // Dimensões da matriz
    int mLinhas;
    int mColunas;

    // Estrutura da matriz
    vector< vector<double> > mMatriz;

public:
    // Construtor
    Matriz(int linhas = 0, int colunas = 0, double value = 0);

    // Retorna dimensões da martriz
    int linhas();
    int colunas();

    // Retorna elemento da posição M_{linha, coluna}
    double get(int linha, int coluna);

    // Seta elemento na posição M_{linha, coluna}
    void set(int linha, int coluna, double valor);

    // Retorna ordem da matriz se ela for quadrada;
    int ordem();

    // Retorna determinante
    double determinante();

    // Retorna matriz transposta
    Matriz transposta();

    // Retorna matriz de cofatores
    Matriz cofatores();

    // Retorna matriz adjunta
    Matriz adjunta();

    // Retorna matriz inversa
    Matriz inversa();

    // Retorna vetor linha com a média de cada coluna
    Matriz media();

    // Retorna matriz de covariância
    Matriz covariancia();

    // Realiza redução da matriz removendo uma linha e uma coluna
    Matriz reducao(int rLinha, int rColuna);

    // Sobrecarga do operador de multiplicação por outra matriz
    Matriz operator*(Matriz M);

    // Sobrecarga do operador de multiplicação por escalar
    Matriz operator*(double d);

    // Sobrecarga do operador de soma
    Matriz operator+(Matriz M);

    // Sobrecarga do operador de subtração
    Matriz operator-(Matriz M);

    // Retorna matriz em formato de string para impressão
    string toString();

    // Método de Jacobi para calcular os autovalores e autovetores da matriz
    void eigen(int iteracoes, double tolerancia, Matriz& autoValores, Matriz& autoVetores);

    Matriz getLinha(int linha);
    Matriz getColuna(int coluna);
    pair<int, int> getMax();
    pair<int, int> getMin();
    Matriz reduzLinha(int rLinha);
    Matriz reduzColuna(int rColuna);

private:
    // Cálculo de determinante quando a matriz é de ordem 2 pela diferença entre
    // a multiplicação da diagonal principal e a multiplicação da diagonal secundária.
    double det_2x2();
    // Cálculo de determinante quando a matriz é de ordem 3 usando Regra de Sarrus
    double det_3x3();
    // Cálculo de determinante quando a matriz é de ordem >3 usando Teorema de Laplace
    double det_nxn();

};

#endif // MATRIZ_H
