#include <iostream>
#include "include/Matriz.hpp"
#include "include/mersenne_twister.h"

using namespace std;

Mersenne mersene;

typedef pair<int,int> state;

enum{
    ACT_UP,
    ACT_LEFT,
    ACT_RIGHT,
    ACT_DOWN
};

enum{
    UP,
    LEFT,
    RIGHT,
    DOWN,

    UP_LEFT,
    UP_RIGHT,
    UP_DOWN,
    LEFT_RIGHT,
    LEFT_DOWN,
    RIGHT_DOWN,

    UP_LEFT_RIGHT,
    UP_LEFT_DOWN,
    UP_RIGHT_DOWN,
    LEFT_RIGHT_DOWN,

    NONE,
    ALL
};

double myround(double x){
    return round(x*10000.0)/10000.0;
}

string mapPolicy(int p){
    string result = "";
    switch (p) {

    case UP:
        result = "▲";
        break;
    case LEFT:
        result = "◄";
        break;
    case RIGHT:
        result = "►";
        break;
    case DOWN:
        result = "▼";
        break;

    case UP_LEFT:
        result = "╝";
        break;
    case UP_RIGHT:
        result = "╚";
        break;
    case UP_DOWN:
        result = "║";
        break;
    case LEFT_RIGHT:
        result = "═";
        break;
    case LEFT_DOWN:
        result = "╗";
        break;
    case RIGHT_DOWN:
        result = "╔";
        break;

    case UP_LEFT_RIGHT:
        result = "╩";
        break;
    case UP_LEFT_DOWN:
        result = "╣";
        break;
    case UP_RIGHT_DOWN:
        result = "╠";
        break;
    case LEFT_RIGHT_DOWN:
        result = "╦";
        break;

    case NONE:
        result = "■";
        break;

    case ALL:
        result = "╬";
        break;

    }
    return result;
}

void printPolicy(Matriz policy){
    for (int linha = 0; linha < policy.linhas(); linha++) {
        for (int coluna = 0; coluna < policy.colunas(); coluna++) {
            cout << mapPolicy(policy.get(linha,coluna)) << " ";
        }
        cout << endl << endl;
    }
    cout << endl;
}

///////////////////////////////////////////////////////

double validValue(Matriz value, state s){
    if(s.first < 0) s.first +=1;
    if(s.second < 0) s.second +=1;
    if(s.first >= value.linhas()) s.first -=1;
    if(s.second >= value.colunas()) s.second -=1;

    return value.get(s.first,s.second);
}

vector<bool> checkTransition(Matriz value, state s, vector<int> actions){

    vector<bool> results;
    vector<int> indices;

    for (unsigned int i = 0; i < actions.size(); i++) {
        results.push_back(false);
    }

    state s1 = s;
    double big = myround(validValue(value, s));

    s1.first = s.first-1;
    s1.second = s.second;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(0);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(0);
    }

    s1.first = s.first;
    s1.second = s.second-1;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(1);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(1);
    }

    s1.first = s.first;
    s1.second = s.second+1;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(2);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(2);
    }

    s1.first = s.first+1;
    s1.second = s.second;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(3);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(3);
    }

    for (unsigned int i = 0; i < indices.size(); i++) {
        results.at(indices.at(i)) = true;
    }

    return results;
}

double policyEvaluation(Matriz value, Matriz reward, double gama, state s){

    double result = 0;
    result += gama*validValue(value, pair<int,int>(s.first-1, s.second));
    result += gama*validValue(value, pair<int,int>(s.first, s.second-1));
    result += gama*validValue(value, pair<int,int>(s.first, s.second+1));
    result += gama*validValue(value, pair<int,int>(s.first+1, s.second));

    result /= 4;
    result += reward.get(s.first, s.second);

    return result;
}

void policyImprovement(Matriz& policy, vector<bool> transitions, state s){

//    vector<bool> transitions =
//            checkTransition(value, s, {ACT_UP,ACT_LEFT,ACT_RIGHT,ACT_DOWN});

    // NONE
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, NONE);

    // UP
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP);

    // LEFT
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, LEFT);

    // RIGHT
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, RIGHT);

    // DOWN
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, DOWN);

    // UP_LEFT
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT);

    // UP_RIGHT
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_RIGHT);

    // UP_DOWN
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_DOWN);

    // LEFT_RIGHT
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, LEFT_RIGHT);

    // LEFT_DOWN
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, LEFT_DOWN);

    // RIGHT_DOWN
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, RIGHT_DOWN);

    // UP_LEFT_RIGHT
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT_RIGHT);

    // UP_LEFT_DOWN
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT_DOWN);

    // UP_RIGHT_DOWN
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_RIGHT_DOWN);

    // LEFT_RIGHT_DOWN
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, LEFT_RIGHT_DOWN);

    // ALL
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, ALL);
}


state nextstate(Matriz policy, state s, int act){

    state s1;
    switch (act) {
    case ACT_UP:
        if(s.first-1 < 0) s1 = pair<int,int>(0,s.second);
        else s1 = pair<int,int>(s.first-1,s.second);

        if(policy.get(s.first,s.second) == UP) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;

        break;
    case ACT_LEFT:
        if(s.second-1 < 0) s1 = pair<int,int>(s.first,0);
        else s1 = pair<int,int>(s.first,s.second-1);

        if(policy.get(s.first,s.second) == LEFT) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == LEFT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;

    case ACT_RIGHT:
        if(s.second+1 >= policy.colunas()) s1 = pair<int,int>(s.first,policy.colunas()-1);
        else s1 = pair<int,int>(s.first,s.second+1);

        if(policy.get(s.first,s.second) == RIGHT) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_RIGHT) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == RIGHT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;

    case ACT_DOWN:
        if(s.first+1 >= policy.linhas()) s1 = pair<int,int>(policy.linhas()-1,s.second);
        else s1 = pair<int,int>(s.first+1,s.second);

        if(policy.get(s.first,s.second) == DOWN) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == RIGHT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;
    }        

    s1 = pair<int,int>(s.first,s.second);
    return s1;
}

int actionGivenBy(Matriz policy, state s){
    int p = policy.get(s.first, s.second);
    vector<int> result;

    switch (p) {

    case UP:
        result.push_back(ACT_UP);
        break;
    case LEFT:
        result.push_back(ACT_LEFT);
        break;
    case RIGHT:
        result.push_back(ACT_RIGHT);
        break;
    case DOWN:
        result.push_back(ACT_DOWN);
        break;

    case UP_LEFT:
        result.push_back(ACT_UP);
        result.push_back(ACT_LEFT);
        break;
    case UP_RIGHT:
        result.push_back(ACT_UP);
        result.push_back(ACT_RIGHT);
        break;
    case UP_DOWN:
        result.push_back(ACT_UP);
        result.push_back(ACT_DOWN);
        break;
    case LEFT_RIGHT:
        result.push_back(ACT_LEFT);
        result.push_back(ACT_RIGHT);
        break;
    case LEFT_DOWN:
        result.push_back(ACT_LEFT);
        result.push_back(ACT_DOWN);
        break;
    case RIGHT_DOWN:
        result.push_back(ACT_RIGHT);
        result.push_back(ACT_DOWN);
        break;

    case UP_LEFT_RIGHT:
        result.push_back(ACT_LEFT);
        result.push_back(ACT_RIGHT);
        break;
    case UP_LEFT_DOWN:
        result.push_back(ACT_UP);
        result.push_back(ACT_LEFT);
        result.push_back(ACT_DOWN);
        break;
    case UP_RIGHT_DOWN:
        result.push_back(ACT_UP);
        result.push_back(ACT_RIGHT);
        result.push_back(ACT_DOWN);
        break;
    case LEFT_RIGHT_DOWN:
        result.push_back(ACT_LEFT);
        result.push_back(ACT_RIGHT);
        result.push_back(ACT_DOWN);
        break;

    case NONE:
        break;

    case ALL:
        result.push_back(ACT_UP);
        result.push_back(ACT_LEFT);
        result.push_back(ACT_RIGHT);
        result.push_back(ACT_DOWN);
        break;

    }


    int rand =  mersene.myRandom(0,result.size());

    if(result.size() == 0){
        return 0;
    }

    return result.at(rand);
}


int e_greedy(state s, vector<Matriz> Q, double e){
    int action = 0;
    double big = 0;
    for (int i = 0; i < 4; i++) {
        if(Q.at(i).get(s.first,s.second) > big){
            big = Q.at(i).get(s.first,s.second);
            action = i;
        }
    }

    if(e < mersene.myRandom()){
        return mersene.myRandom(0,4);
    }else{
        return action;
    }
}

Matriz getPoliceFromQ(vector<Matriz> Q){
    int maxLinha = Q.at(0).linhas();
    int maxColuna = Q.at(0).colunas();
    Matriz policy = Matriz(maxLinha, maxColuna, NONE);

    vector<int> ind;
    vector<bool> transitions;

    for (int linha = 0; linha < maxLinha; linha++) {
        for (int coluna = 0; coluna < maxColuna; coluna++) {

            ind.clear();
            transitions.clear();
            for (unsigned int i = 0; i < 4; i++) {
                transitions.push_back(false);
            }

            double big = -100000;

            for (int i = 0; i < 4; i++) {
                double v = Q.at(i).get(linha,coluna);

                if(v > big){
                    big = v;
                    ind.clear();
                    ind.push_back(i);
                }else if(v == big){
                    ind.push_back(i);
                }
            }

            for (unsigned int i = 0; i < ind.size(); i++) {
                transitions.at(ind.at(i)) = true;
            }

            policyImprovement(policy, transitions, pair<int,int>(linha,coluna));

        }
    }

    return policy;
}

int main(int argc, char *argv[])
{
    // parameters
    int maxLinha = 6;
    int maxColuna = 6;
    double alfa = 0.8;
    double gama = 0.8;
    int maxEpisodes = 10000;

    // final points
    vector<pair<int,int>> finalPoints =
    {
        // corner
//        pair<int,int>(0,0),
//        pair<int,int>(maxLinha-1,maxColuna-1),
        // randon
        pair<int,int>(mersene.myRandom(maxLinha),mersene.myRandom(maxColuna)),
        pair<int,int>(mersene.myRandom(maxLinha),mersene.myRandom(maxColuna)),
    };

    // init gridword
    Matriz gridword = Matriz(maxLinha, maxColuna);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        gridword.set(finalPoints.at(i).first,finalPoints.at(i).second,1);
    }
    cout << "gridword" << endl;
    cout << gridword.toString() << endl;

    // init reward
    Matriz reward = Matriz(maxLinha, maxColuna, -1);
    reward = reward + gridword;
    cout << "reward" << endl;
    cout << reward.toString() << endl;

    /// TD(0) ///
    const clock_t begin_time = clock();

    // Input: the policy π to be evaluated
    Matriz policy = Matriz(maxLinha, maxColuna, ALL);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        policy.set(finalPoints.at(i).first,finalPoints.at(i).second,NONE);
    }
    cout << "policy" << endl;
    printPolicy(policy);

    // Initialize V (s) arbitrarily (e.g., V (s) = 0, for all s ∈ S+)
    Matriz value = Matriz(maxLinha, maxColuna);

    // Repeat (for each episode):
    for (int episode = 0; episode < maxEpisodes; episode++) {

        // Initialize S
        state s = pair<int, int>(mersene.myRandom(maxLinha), mersene.myRandom(maxColuna));

        // Repeat (for each step of episode) until S is terminal
        while (gridword.get(s.first,s.second) == 0) {

            // A ← action given by π for S
            int a = actionGivenBy(policy,s);

            // take action A, observe r, s'
            state s1 = nextstate(policy, s, a);
            double r = reward.get(s.first, s.second);

            // V(S) ← V(S) + α[R + γV(S') − V(S)]
            double v = value.get(s.first,s.second) + alfa *
                              (r + gama * value.get(s1.first,s1.second) - value.get(s.first,s.second));
            value.set(s.first,s.second, v);


            vector<bool> transitions =
                    checkTransition(value, s, {ACT_UP,ACT_LEFT,ACT_RIGHT,ACT_DOWN});
            policyImprovement(policy, transitions, s);

            // s <- s'
            s = s1;

        }
    }
    std::cout << "Tempo: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " segundos." <<endl;
    printPolicy(policy);

    return 0;
}

