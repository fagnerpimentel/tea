#include <iostream>
#include <fstream>

#include "include/Util.hpp"
#include "include/MachineLearning.hpp"

using namespace std;

int main(int argc, char *argv[])
{

    /** Leitura dos dados do arquivo **/
    Matriz dados = readFile(argv[1]);
    cout << "Dados:" << endl;
    cout << dados.toString() << endl;

    /*** O método PCA ***/
    PCA(dados, 0.98);

    return 0;
}

