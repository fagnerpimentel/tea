#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>

#include "include/Util.hpp"
#include "include/MachineLearning.hpp"
#include "include/mersenne_twister.h"

using namespace std;

double euclidian(Matriz p1, Matriz p2){
    if(p1.colunas() != p2.colunas()){
        throw std::length_error( "euclidian - Os pontos precisam ter a mesma dimensão" );
    }

    double soma = 0;
    for (int coluna = 0; coluna < p1.colunas(); coluna++) {
        soma += pow(p1.get(0,coluna) - p2.get(0,coluna),2);
    }

    return sqrt(soma);
}
double manhattan(Matriz p1, Matriz p2){
    if(p1.colunas() != p2.colunas()){
        throw std::length_error( "manhattan - Os pontos precisam ter a mesma dimensão" );
    }

    double soma = 0;
    for (int coluna = 0; coluna < p1.colunas(); coluna++) {
        soma += fabs(p1.get(0,coluna) - p2.get(0,coluna));
    }

    return soma;
}

int main(int argc, char *argv[])
{

    string param_1 = argv[1];
    int param_2 = boost::lexical_cast<int>(argv[2]);
    int param_3 = boost::lexical_cast<int>(argv[3]);

    if(param_3 != 1 && param_3 != 2){
        cout << "São aceitos apenas os valores '1' para distância Euclinada e '2' para distância Manhattan." << endl;
        exit(1);
    }

    /** Leitura dos dados do arquivo **/
    Matriz dados = readFile(argv[1]);
    cout << "Dados:" << endl;
    dados = dados.reduzColuna(0);
    cout << dados.toString() << endl;

//    Matriz dados = Matriz(100, 2);
//    for (int linha = 0; linha < dados.linhas(); linha++) {
//        for (int coluna = 0; coluna < dados.colunas(); coluna++) {
//            dados.set(linha, coluna, myRandom(100));
//        }
//    }
//    cout << dados.toString() << endl;


    /*** O método KMeans ***/    

    /** Parâmetros **/
    int k = param_2;            cout << "Número de Ks: " << k << endl;
    int it = 50;                cout << "Número máximo de iterações: " << it << endl;
    int d = dados.colunas();    cout << "Número de dimensões: " << d << endl;
    int n = dados.linhas();     cout << "Número de exemplos: " << n << endl;
    int distType = param_3;
    cout << endl;

    // vetor com historico do erro a cada iteraçao
    vector<double> history_erro;

    /** Inicia k pontos randômicos **/
    Matriz k_atual = Matriz(k,d);  //posições atuais dos Ks
    Matriz k_antiga = Matriz(k,d); //posições antigas dos Ks
    for (int coluna = 0; coluna < k_atual.colunas(); coluna++) {
        pair<int, int> max = dados.getColuna(coluna).getMax();
        pair<int, int> min = dados.getColuna(coluna).getMin();
        for (int linha = 0; linha < k_atual.linhas(); linha++) {
            k_atual.set(linha, coluna, myRandom(
                            dados.getColuna(coluna).get(min.first, min.second),
                            dados.getColuna(coluna).get(max.first, max.second)));
        }
    }
    k_antiga = k_atual;
    cout << "K iniciais:" << endl;
    cout << k_atual.toString() << endl;

    /** Iterações **/
    for (int _it = 0; _it < it; _it++) {
        cout << "Iteração "<< _it+1 << ":" << endl;

        /** Calcula distâncias **/
        // varre dados
        Matriz dist = Matriz(n, k);
        for (int _n = 0; _n < n; _n++) {
            // varre Ks
            for (int _k = 0; _k < k; _k++) {
                // calcula distâncias de cada k para cada ponto
                if(distType == 1)dist.set(_n, _k, euclidian(k_atual.getLinha(_k), dados.getLinha(_n)));
                if(distType == 2)dist.set(_n, _k, manhattan(k_atual.getLinha(_k), dados.getLinha(_n)));
            }
        }
        cout << "Distâncias:" << endl;
        cout << dist.toString() << endl;

        /** Agrupa postos mais proxiso de cada K **/
        cout << "Agrupamento dos pontos:" << endl;
        // agrupa
        vector<vector<int> > k_pontos = vector<vector<int> >(k);
        for (int _n = 0; _n < n; _n++) {
            pair<int, int> index = dist.getLinha(_n).getMin();
            k_pontos.at(index.second).push_back(_n);
        }
        // imprime
        for (unsigned int i = 0; i < k_pontos.size(); i++) {
            cout << "k " << i+1 << ": ";
            vector<int> v = k_pontos.at(i);
            for (unsigned int j = 0; j < v.size(); j++) {
                cout << v.at(j) << " ";
            }
            cout << endl;
        }
        cout << endl;

        /** Atualização dos Ks **/
        Matriz k_novo = Matriz(k,d);
        for (int _k = 0; _k < k; _k++) {
            vector<int> v = k_pontos.at(_k);

            for (int _d = 0; _d < d; ++_d) {
                if(v.size() == 0){
                    k_novo.set(_k, _d, k_atual.get(_k, _d));
                }else{
                    for (unsigned int i = 0; i < v.size(); ++i) {
                        k_novo.set(_k, _d, k_novo.get(_k,_d) + dados.get(v.at(i), _d)/v.size());
                    }
                }
            }
        }
        k_antiga = k_atual;
        k_atual = k_novo;
        cout << "Ks atualizados:" << endl;
        cout << k_atual.toString() << endl;

        /** Erro **/
        cout << "Erro:" << endl;
        Matriz erro = k_atual - k_antiga;
        cout << erro.toString() << endl;

        // Histórico do erro
        double valor_erro = erro.transposta().media().transposta().media().get(0,0);
        history_erro.push_back(valor_erro);
        for (unsigned int i = 0; i < history_erro.size(); i++) {
            cout << history_erro.at(i) << endl;
        }

        if(valor_erro == 0){
            cout << "Convergência atingida com " << _it+1 << " iterações." << endl;
            return 0;
        }
    }

    cout << "Número máximo de iterações atingido." << endl;
    return 0;
}

