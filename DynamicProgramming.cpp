#include <iostream>
#include "include/Matriz.hpp"

using namespace std;

double myround(double x){
    return round(x*10000.0)/10000.0;
}

enum{
    ACT_UP,
    ACT_LEFT,
    ACT_RIGHT,
    ACT_DOWN
};

enum{
    NONE,

    UP,
    LEFT,
    RIGHT,
    DOWN,

    UP_LEFT,
    UP_RIGHT,
    UP_DOWN,
    LEFT_RIGHT,
    LEFT_DOWN,
    RIGHT_DOWN,

    UP_LEFT_RIGHT,
    UP_LEFT_DOWN,
    UP_RIGHT_DOWN,
    LEFT_RIGHT_DOWN,

    ALL
};

double validValue(Matriz M, int linha, int coluna){
    if(linha < 0) linha +=1;
    if(coluna < 0) coluna +=1;
    if(linha >= M.linhas()) linha -=1;
    if(coluna >= M.colunas()) coluna -=1;

    return M.get(linha,coluna);
}

string validPolicy(int p){
    string result = "";
    switch (p) {
    case NONE:
        result = "■";
        break;

    case UP:
        result = "▲";
        break;
    case LEFT:
        result = "◄";
        break;
    case RIGHT:
        result = "►";
        break;
    case DOWN:
        result = "▼";
        break;

    case UP_LEFT:
        result = "╝";
        break;
    case UP_RIGHT:
        result = "╚";
        break;
    case UP_DOWN:
        result = "║";
        break;
    case LEFT_RIGHT:
        result = "═";
        break;
    case LEFT_DOWN:
        result = "╗";
        break;
    case RIGHT_DOWN:
        result = "╔";
        break;

    case UP_LEFT_RIGHT:
        result = "╩";
        break;
    case UP_LEFT_DOWN:
        result = "╣";
        break;
    case UP_RIGHT_DOWN:
        result = "╠";
        break;
    case LEFT_RIGHT_DOWN:
        result = "╦";
        break;

    case ALL:
        result = "╬";
        break;

    }
    return result;
}

vector<bool> checkTransition(Matriz value, int linha, int coluna, vector<int> actions){

    vector<bool> results;
    vector<int> indices;

    for (unsigned int i = 0; i < actions.size(); i++) {
        results.push_back(false);
    }

    double big = myround(validValue(value, linha, coluna));

    if(myround(validValue(value, linha-1, coluna)) > myround(big)){
        indices.clear();
        indices.push_back(0);
        big = validValue(value, linha-1, coluna);
    }else if(myround(validValue(value, linha-1, coluna)) == myround(big)){
        indices.push_back(0);
    }

    if(myround(validValue(value, linha, coluna-1)) > myround(big)){
        indices.clear();
        indices.push_back(1);
        big = validValue(value, linha, coluna-1);
    }else if(myround(validValue(value, linha, coluna-1)) == myround(big)){
        indices.push_back(1);
    }

    if(myround(validValue(value, linha, coluna+1)) > myround(big)){
        indices.clear();
        indices.push_back(2);
        big = validValue(value, linha, coluna+1);
    }else if(myround(validValue(value, linha, coluna+1)) == myround(big)){
        indices.push_back(2);
    }

    if(myround(validValue(value, linha+1, coluna)) > myround(big)){
        indices.clear();
        indices.push_back(3);
        big = validValue(value, linha+1, coluna);
    }else if(myround(validValue(value, linha+1, coluna)) == myround(big)){
        indices.push_back(3);
    }

    for (unsigned int i = 0; i < indices.size(); i++) {
        results.at(indices.at(i)) = true;
    }


    return results;
}

void printPolicy(Matriz policy){
    for (int linha = 0; linha < policy.linhas(); linha++) {
        for (int coluna = 0; coluna < policy.colunas(); coluna++) {
            cout << validPolicy(policy.get(linha,coluna)) << " ";
        }
        cout << endl << endl;
    }
    cout << endl;
}


double policyEvaluation(double gama, Matriz reward, Matriz value, int linha, int coluna){

    double result = 0;
    result += gama*validValue(value, linha-1, coluna);
    result += gama*validValue(value, linha, coluna-1);
    result += gama*validValue(value, linha, coluna+1);
    result += gama*validValue(value, linha+1, coluna);

    result /= 4;
    result += reward.get(linha, coluna);

    return result;
}

void policyImprovement(Matriz& policy, Matriz value, int linha, int coluna){

    vector<bool> transitions =
            checkTransition(value, linha, coluna, {ACT_UP,ACT_LEFT,ACT_RIGHT,ACT_DOWN});

    // NONE
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, NONE);

    // UP
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, UP);

    // LEFT
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, LEFT);

    // RIGHT
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, RIGHT);

    // DOWN
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, DOWN);

    // UP_LEFT
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, UP_LEFT);

    // UP_RIGHT
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, UP_RIGHT);

    // UP_DOWN
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, UP_DOWN);

    // LEFT_RIGHT
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, LEFT_RIGHT);

    // LEFT_DOWN
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, LEFT_DOWN);

    // RIGHT_DOWN
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, RIGHT_DOWN);

    // UP_LEFT_RIGHT
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(linha, coluna, UP_LEFT_RIGHT);

    // UP_LEFT_DOWN
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, UP_LEFT_DOWN);

    // UP_RIGHT_DOWN
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, UP_RIGHT_DOWN);

    // LEFT_RIGHT_DOWN
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, LEFT_RIGHT_DOWN);

    // ALL
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(linha, coluna, ALL);
}

int main(int argc, char *argv[])
{

    // parameters
    int maxLinha = 6;
    int maxColuna = 6;
    double theta = 0.2;
    double gama = 1;
    vector<pair<int,int>> finalPoints =
    {
        pair<int,int>(0,0),
        pair<int,int>(maxLinha-1,maxColuna-1)
//        pair<int,int>(5,1),
//        pair<int,int>(4,4)
    };

    // init gridword
    Matriz gridword = Matriz(maxLinha, maxColuna);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        gridword.set(finalPoints.at(i).first,finalPoints.at(i).second,1);
    }
    cout << "gridword" << endl;
    cout << gridword.toString() << endl;

    // init reward
    Matriz reward = Matriz(maxLinha, maxColuna, -1);
    reward = reward + gridword;
    cout << "reward" << endl;
    cout << reward.toString() << endl;

    // init value
    Matriz value = Matriz(maxLinha, maxColuna);
    cout << "value" << endl;
    cout << value.toString() << endl;

    // init policy
    Matriz policy = Matriz(maxLinha, maxColuna, NONE);
    cout << "policy" << endl;
    printPolicy(policy);

    // stop condition
    double delta;

    Matriz x = Matriz(maxLinha, maxColuna);
    int it = 0;
    do{
        it++;
        cout << "iteration " << it <<  endl << endl;

        delta = 0;
        for (int linha = 0; linha < maxLinha; linha++) {
            for (int coluna = 0; coluna < maxColuna; coluna++) {
                if(gridword.get(linha, coluna) == 1) continue;
                double v = value.get(linha, coluna);
                policyImprovement(policy, value, linha, coluna);
                double result = policyEvaluation(gama, reward, value, linha, coluna);
                x.set(linha, coluna, result);
                delta = max(delta,v-result);
            }
        }
        value = x;

        cout << "value" << endl;
        cout << value.toString() << endl;

        cout << "policy" << endl;
        printPolicy(policy);

//        cout << "delta: " << delta << endl;
//        cout << "theta: " << theta << endl;
//        cout << endl;

//        getchar();
    }while(delta > theta);

    return 0;
}

