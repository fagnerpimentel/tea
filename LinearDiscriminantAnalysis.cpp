#include <iostream>
#include <fstream>

#include "include/MachineLearning.hpp"
#include "include/Util.hpp"
#include "include/eigen_unsym.h"


using namespace std;

int main(int argc, char *argv[])
{

    /** Dados do arquivo **/
    cout << "Dados do arquivo:" << endl;
    Matriz dados = readFile(argv[1]);
    cout << dados.toString() << endl;

    /** Seleçao do tipo de método**/

    cout << "Escolha o tipo de método:" << endl;
    cout << "1 - LDA." << endl;
    cout << "2 - MDF = PCA + LDA." << endl << endl;
    int tipo = 0;
    cin >> tipo;
    bool usePCA = false;

    cout << "Tipo escolhido: ";
    switch (tipo) {
    case 1:
        cout << "LDA." << endl << endl;
        usePCA = false;
        break;
    case 2:
        cout << "MDF." << endl << endl;
        usePCA = true;
        break;
    default:
        cout << "Opção não disponível" << endl << endl;
        exit(1);
        break;
    }

    /*** O método LDA ***/

    /** Dados gerais **/
    cout << "Dados gerais:" << endl << endl;
    Matriz geral_x = Matriz(dados.linhas(), dados.colunas() - 1);
    Matriz geral_y = Matriz(dados.linhas(), 1);
    for (int linha = 0; linha < dados.linhas(); linha++) {
        geral_y.set(linha, 0, dados.get(linha, 0)); //
        for (int coluna = 1; coluna < dados.colunas(); coluna++) {
            geral_x.set(linha, coluna - 1, dados.get(linha, coluna) * 0.1); //
        }
    }
    cout << geral_x.toString() << endl;

    if(usePCA){
        /** PCA dos dados **/
        Matriz pca = PCA(geral_x, 0.98);
        geral_x = pca;
    }

    /** Dados por classes **/
    cout << "Dados por classe:" << endl << endl;

    // determina número de classes nos dados de entrada
    int numeroDeClasses = 0;
    for (int linha = 0; linha < dados.linhas(); linha++) {
        if(geral_y.get(linha, 0) >= numeroDeClasses) numeroDeClasses = geral_y.get(linha, 0) + 1;
    }

    // inicializa vetor com uma matriz para cada classe
    vector<vector<vector<double> > > vClasse;
    for (int i = 0; i < numeroDeClasses; i++) {
        vector<vector<double> > vMatriz;
        vClasse.push_back(vMatriz);
    }

    // separa os dados de entrada
    for (int linha = 0; linha < geral_x.linhas(); linha++) {
        vector<double> vLinha;
        for (int coluna = 0; coluna < geral_x.colunas(); coluna++) {
            vLinha.push_back(geral_x.get(linha, coluna));
        }
        int classe = geral_y.get(linha, 0);
        vClasse.at(classe).push_back(vLinha);
    }

    vector<Matriz> datasets;
    for (int i = 0; i < numeroDeClasses; i++) {
        Matriz m = Matriz(vClasse.at(i).size(),vClasse.at(i).at(0).size());
        for (int linha = 0; linha < m.linhas(); linha++) {
            for (int coluna = 0; coluna < m.colunas(); coluna++) {
                m.set(linha, coluna, vClasse.at(i).at(linha).at(coluna));
            }
        }
        datasets.push_back(m); //
        cout << "Dados da classe " << i << ":" << endl;
        cout << datasets.at(i).toString() << endl;
    }

    /** Média geral **/
    cout << "Média geral:" << endl;
    Matriz mediaGeral = geral_x.media();
    cout << mediaGeral.toString() << endl;

    /** Média das classes **/
    cout << "Média das classes" << endl << endl;
    vector<Matriz> mediasDosDatasets;
    for (int i = 0; i < numeroDeClasses; i++) {
        mediasDosDatasets.push_back(datasets.at(i).media());
        cout << "Média da classe " << i << ":" << endl;
        cout << mediasDosDatasets.at(i).toString() << endl;
    }

    /** within-class scatter matrix Sw **/
    cout << "within-class scatter matrix Sw:" << endl;
    Matriz Sw = Matriz(geral_x.colunas(), geral_x.colunas());
    for (int c = 0; c < numeroDeClasses; c++) {
        for (int linha = 0; linha < datasets.at(c).linhas(); linha++) {
            Matriz aux = Matriz(1, datasets.at(c).colunas());
            for (int coluna = 0; coluna < datasets.at(c).colunas(); coluna++) {
                aux.set(0, coluna, datasets.at(c).get(linha, coluna));
            }
            Sw = Sw + ((aux - mediasDosDatasets.at(c)).transposta()
                    * (aux - mediasDosDatasets.at(c)));
        }
    }
    cout << Sw.toString() << endl;

    /** between-class scatter matrix Sb **/
    cout << "between-class scatter matrix Sb:" << endl;
    Matriz Sb = Matriz(geral_x.colunas(), geral_x.colunas());
    for (int c = 0; c < numeroDeClasses; c++) {
        Matriz diferencaDaMedia = mediasDosDatasets.at(c) - mediaGeral;
        Sb = Sb + (diferencaDaMedia.transposta() * diferencaDaMedia) * datasets.at(c).linhas();
    }
    cout << Sb.toString() << endl;

    /** inv(Sw)*Sb **/
    Matriz R = Sw.inversa() * Sb;
    cout << "inv(Sw)*Sb:" << endl;
    cout << R.toString() << endl;

    /** Calcula auto valores e auto vetores **/
    MatDoub a(R.linhas(), R.colunas());
    for (int linha = 0; linha < R.linhas(); linha++) {
        for (int coluna = 0; coluna < R.colunas(); coluna++) {
            a[linha][coluna] = R.get(linha, coluna);
        }
    }
    Unsymmeig h(a, true);

    cout << "Auto valores:" << endl;
    VecComplex eigenvectors = h.wri;
    for (int i = 0; i < eigenvectors.size(); ++i) {
        cout << eigenvectors[i] << endl;
    }

    cout << "Auto vetores:" << endl;
    MatDoub vec = h.zz;
    Matriz mvec = Matriz(vec.nrows(), vec.ncols());
    for (int linha = 0; linha < vec.nrows(); linha++) {
        for (int coluna = 0; coluna < vec.ncols(); coluna++) {
            mvec.set(linha, coluna, vec[linha][coluna]);
        }
    }
    cout << mvec.toString() << endl;

    /** projection matrix Pmva **/


    return 0;
}
