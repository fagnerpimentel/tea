#include <iostream>
#include "include/Matriz.hpp"
#include "include/mersenne_twister.h"

//#include <boost/tuple/tuple.hpp>
//#include "../gnuplot-iostream/gnuplot-iostream.h"

using namespace std;

Mersenne mersene;

typedef pair<int,int> state;

enum{
    ACT_UP,
    ACT_LEFT,
    ACT_RIGHT,
    ACT_DOWN
};

enum{
    UP,
    LEFT,
    RIGHT,
    DOWN,

    UP_LEFT,
    UP_RIGHT,
    UP_DOWN,
    LEFT_RIGHT,
    LEFT_DOWN,
    RIGHT_DOWN,

    UP_LEFT_RIGHT,
    UP_LEFT_DOWN,
    UP_RIGHT_DOWN,
    LEFT_RIGHT_DOWN,

    NONE,
    ALL
};

Matriz gridword = Matriz();
Matriz reward = Matriz();

Matriz value = Matriz();
Matriz policy = Matriz();


Matriz policyRandon = Matriz();
Matriz valueSum = Matriz();
Matriz statePassCount = Matriz();

Matriz valueMC = Matriz();
Matriz policyMC = Matriz();

double myround(double x){
    return round(x*10000.0)/10000.0;
}

string mapPolicy(int p){
    string result = "";
    switch (p) {

    case UP:
        result = "▲";
        break;
    case LEFT:
        result = "◄";
        break;
    case RIGHT:
        result = "►";
        break;
    case DOWN:
        result = "▼";
        break;

    case UP_LEFT:
        result = "╝";
        break;
    case UP_RIGHT:
        result = "╚";
        break;
    case UP_DOWN:
        result = "║";
        break;
    case LEFT_RIGHT:
        result = "═";
        break;
    case LEFT_DOWN:
        result = "╗";
        break;
    case RIGHT_DOWN:
        result = "╔";
        break;

    case UP_LEFT_RIGHT:
        result = "╩";
        break;
    case UP_LEFT_DOWN:
        result = "╣";
        break;
    case UP_RIGHT_DOWN:
        result = "╠";
        break;
    case LEFT_RIGHT_DOWN:
        result = "╦";
        break;

    case NONE:
        result = "■";
        break;

    case ALL:
        result = "╬";
        break;

    }
    return result;
}

void printPolicy(Matriz policy){
    for (int linha = 0; linha < policy.linhas(); linha++) {
        for (int coluna = 0; coluna < policy.colunas(); coluna++) {
            cout << mapPolicy(policy.get(linha,coluna)) << " ";
        }
        cout << endl << endl;
    }
    cout << endl;
}

///////////////////////////////////////////////////////

double validValue(Matriz value, state s){
    if(s.first < 0) s.first +=1;
    if(s.second < 0) s.second +=1;
    if(s.first >= value.linhas()) s.first -=1;
    if(s.second >= value.colunas()) s.second -=1;

    return value.get(s.first,s.second);
}

vector<bool> checkTransition(Matriz value, state s, vector<int> actions){

    vector<bool> results;
    vector<int> indices;

    for (unsigned int i = 0; i < actions.size(); i++) {
        results.push_back(false);
    }

    state s1 = s;
    double big = myround(validValue(value, s));

    s1.first = s.first-1;
    s1.second = s.second;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(0);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(0);
    }

    s1.first = s.first;
    s1.second = s.second-1;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(1);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(1);
    }

    s1.first = s.first;
    s1.second = s.second+1;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(2);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(2);
    }

    s1.first = s.first+1;
    s1.second = s.second;
    if(myround(validValue(value, s1)) == myround(value.get(s.first,s.second))){
    } else if(myround(validValue(value, s1)) > myround(big)){
        indices.clear();
        indices.push_back(3);
        big = validValue(value, s1);
    }else if(myround(validValue(value, s1)) == myround(big)){
        indices.push_back(3);
    }

    for (unsigned int i = 0; i < indices.size(); i++) {
        results.at(indices.at(i)) = true;
    }

    return results;
}

double policyEvaluation(Matriz value, double gama, state s){

    double result = 0;
    result += gama*validValue(value, pair<int,int>(s.first-1, s.second));
    result += gama*validValue(value, pair<int,int>(s.first, s.second-1));
    result += gama*validValue(value, pair<int,int>(s.first, s.second+1));
    result += gama*validValue(value, pair<int,int>(s.first+1, s.second));

    result /= 4;
    result += reward.get(s.first, s.second);

    return result;
}

void policyImprovement(Matriz& policy, Matriz value, state s){

    vector<bool> transitions =
            checkTransition(value, s, {ACT_UP,ACT_LEFT,ACT_RIGHT,ACT_DOWN});

    // NONE
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, NONE);

    // UP
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP);

    // LEFT
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, LEFT);

    // RIGHT
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, RIGHT);

    // DOWN
    if(!transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, DOWN);

    // UP_LEFT
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT);

    // UP_RIGHT
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_RIGHT);

    // UP_DOWN
    if(transitions.at(0) && !transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_DOWN);

    // LEFT_RIGHT
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, LEFT_RIGHT);

    // LEFT_DOWN
    if(!transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, LEFT_DOWN);

    // RIGHT_DOWN
    if(!transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, RIGHT_DOWN);

    // UP_LEFT_RIGHT
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && !transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT_RIGHT);

    // UP_LEFT_DOWN
    if(transitions.at(0) && transitions.at(1) && !transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_LEFT_DOWN);

    // UP_RIGHT_DOWN
    if(transitions.at(0) && !transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, UP_RIGHT_DOWN);

    // LEFT_RIGHT_DOWN
    if(!transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, LEFT_RIGHT_DOWN);

    // ALL
    if(transitions.at(0) && transitions.at(1) && transitions.at(2) && transitions.at(3))
        policy.set(s.first, s.second, ALL);
}


state nextstate(Matriz policy, state s, int act){

    state s1;
    switch (act) {
    case ACT_UP:
        if(s.first-1 < 0) s1 = pair<int,int>(0,s.second);
        else s1 = pair<int,int>(s.first-1,s.second);

        if(policy.get(s.first,s.second) == UP) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;

        break;
    case ACT_LEFT:
        if(s.second-1 < 0) s1 = pair<int,int>(s.first,0);
        else s1 = pair<int,int>(s.first,s.second-1);

        if(policy.get(s.first,s.second) == LEFT) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == LEFT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;

    case ACT_RIGHT:
        if(s.second+1 >= policy.colunas()) s1 = pair<int,int>(s.first,policy.colunas()-1);
        else s1 = pair<int,int>(s.first,s.second+1);

        if(policy.get(s.first,s.second) == RIGHT) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_RIGHT) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == RIGHT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_RIGHT) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;

    case ACT_DOWN:
        if(s.first+1 >= policy.linhas()) s1 = pair<int,int>(policy.linhas()-1,s.second);
        else s1 = pair<int,int>(s.first+1,s.second);

        if(policy.get(s.first,s.second) == DOWN) return s1;
        if(policy.get(s.first,s.second) == ALL) return s1;

        if(policy.get(s.first,s.second) == UP_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == RIGHT_DOWN) return s1;

        if(policy.get(s.first,s.second) == UP_LEFT_DOWN) return s1;
        if(policy.get(s.first,s.second) == UP_RIGHT_DOWN) return s1;
        if(policy.get(s.first,s.second) == LEFT_RIGHT_DOWN) return s1;

        break;
    }

    s1 = pair<int,int>(s.first,s.second);
    return s1;
}

int main(int argc, char *argv[])
{
    // gnuplot
//    Gnuplot gp("tee plot.gp | gnuplot -persist");
//    std::vector<std::pair<double, double> > xy_pts_A;

    // parameters
    int maxLinha = 6;
    int maxColuna = 6;
    double theta = 0.1;
    double gama = 1;
    vector<pair<int,int>> finalPoints =
    {
        // corner
        pair<int,int>(0,0),
        pair<int,int>(maxLinha-1,maxColuna-1),
        // randon
        //pair<int,int>(mersene.myRandom(maxLinha),mersene.myRandom(maxColuna)),
        //pair<int,int>(mersene.myRandom(maxLinha),mersene.myRandom(maxColuna)),
    };

    // init gridword
    gridword = Matriz(maxLinha, maxColuna);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        gridword.set(finalPoints.at(i).first,finalPoints.at(i).second,1);
    }
    cout << "gridword" << endl;
    cout << gridword.toString() << endl;

    // init reward
    reward = Matriz(maxLinha, maxColuna, -1);
    reward = reward + gridword;
    cout << "reward" << endl;
    cout << reward.toString() << endl;

    // init value
    value = Matriz(maxLinha, maxColuna);
    cout << "value" << endl;
    cout << value.toString() << endl;

    // init policy
    policy = Matriz(maxLinha, maxColuna, ALL);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        policy.set(finalPoints.at(i).first,finalPoints.at(i).second,NONE);
    }
    cout << "policy" << endl;
    printPolicy(policy);

    // stop condition
    double delta;

    Matriz x = Matriz(maxLinha, maxColuna);
    int it = 0;
    do{
        it++;
        cout << "iteration " << it <<  endl << endl;

        delta = 0;
        for (int linha = 0; linha < maxLinha; linha++) {
            for (int coluna = 0; coluna < maxColuna; coluna++) {
                state s = pair<int,int>(linha, coluna);
                if(gridword.get(linha, coluna) == 1) continue;
                double v = value.get(linha, coluna);
                policyImprovement(policy, value, s);
                double result = policyEvaluation(value, gama, s);
                x.set(linha, coluna, result);
                delta = max(delta,v-result);
            }
        }
        value = x;

        cout << "value" << endl;
        cout << value.toString() << endl;

        cout << "policy" << endl;
        printPolicy(policy);

//        getchar();
    }while(delta > theta);


    /// MONTE CARLO ///


    // init value MC
    valueMC = Matriz(maxLinha, maxColuna);
    cout << "value MC" << endl;
    cout << valueMC.toString() << endl;

    // init policy MC
    policyMC = Matriz(maxLinha, maxColuna, ALL);
    for (unsigned int i = 0; i < finalPoints.size(); i++) {
        policyMC.set(finalPoints.at(i).first,finalPoints.at(i).second,NONE);
    }

    // init policy randon MC
    policyRandon = policyMC;
    valueSum = Matriz(maxLinha, maxColuna, 0);
    statePassCount = Matriz(maxLinha, maxColuna, 0);

    cout << "policy MC" << endl;
    printPolicy(policyMC);

    // init returns MC
    vector<state> returns;

    const clock_t begin_time = clock();

    int count = 0;
    while (count < 10000) {
        count++;
        int linha = mersene.myRandom(maxLinha);
        int coluna = mersene.myRandom(maxColuna);
        state s = pair<int, int>(linha, coluna);
        returns.clear();
        returns.push_back(s);
        if (gridword.get(s.first,s.second) == 1) continue;

        while (gridword.get(s.first,s.second) == 0) {
           s = nextstate(policyRandon, s, (int)mersene.myRandom(4));
           returns.push_back(s);
        }


        for (unsigned int i = 0; i < returns.size(); i++) {
            cout << "(" << returns.at(i).first << "," << returns.at(i).second << ") -> ";
        }
        cout << endl;

        for (unsigned int i = 0; i < returns.size(); i++) {
            s = returns.at(i);
            double v = (signed int) -(returns.size()-1-i);
            valueSum.set(s.first, s.second, valueSum.get(s.first, s.second) + v);
            statePassCount.set(s.first, s.second, statePassCount.get(s.first, s.second)+1);
            valueMC.set(s.first,s.second, valueSum.get(s.first, s.second)/statePassCount.get(s.first, s.second));
            policyImprovement(policyMC, valueMC, s);
        }


        cout << endl;
        cout << "random: " << "(" << linha << "," << coluna << ")" << endl;

        cout << "value sum" << endl;
        cout << valueSum.toString() << endl;

        cout << "state count" << endl;
        cout << statePassCount.toString() << endl;

        cout << "value MC:" << endl;
        cout << valueMC.toString() << endl;

        cout << "policy MC:" << endl;
        printPolicy(policyMC);

        cout << "error: " ;
        double error = abs((value-valueMC).media().transposta().media().get(0,0));
        cout << error << endl;

//        xy_pts_A.push_back(std::make_pair(count, error));
//        gp << "plot" << gp.file1d(xy_pts_A) << "with lines title 'erro médio'," << std::endl;
    }
    std::cout << "Tempo: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << " segundos." << endl << endl;

    cout << "policy:" << endl;
    printPolicy(policy);

    return 0;
}

