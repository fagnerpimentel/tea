#include <iostream>
#include <fstream>
#include "include/Matriz.hpp"
#include "include/rna.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    // variaveis de leitura
    string line; // armazena cada linha do arquivo
    string element; // armazena cada elemento das linhas


    // parametros da rna
    int n_entrada = 16*16;
    int n_saida = 10;
    int n_escondida = 30;
    int camadas_escondidas = 2;
    int maximo_iteracoes = 500;
    double erro_minimo = 0.1;
    double taxa_aprendizado = 0.2;

    cout << "Entre com o numero de saidas:" << endl;
    cin >> n_saida;

    cout << "Entre com o numero de camadas:" << endl;
    cin >> camadas_escondidas;

    cout << "Entre com o numero de nourônios:" << endl;
    cin >> n_escondida;

    RNA rna = RNA(n_entrada, n_saida, n_escondida, camadas_escondidas,
                  maximo_iteracoes, erro_minimo, taxa_aprendizado);

    cout << rna.info() << endl;


    // matriz de entrada
    Matriz entrada = Matriz(1, n_entrada+1);

    /** Train **/
    int num_train = 0;
    const clock_t begin_time_train = clock();

    // Abre aquivo
    ifstream file_train (argv[1]);
    if (file_train.is_open()) {
        // Varre aquivo
        while (!file_train.eof()) {
            getline (file_train,line);
            istringstream iss(line);

            // Varre linhas
            int coluna = 0;
            while(iss >> element) {
                // Armazena cada elemento das linhas em um vetor
                double value = atof(element.c_str());
                entrada.set(0, coluna, value);
                coluna++;
            }


            if(entrada.get(0,0) >= n_saida) continue;
            num_train++;
            rna.train(entrada);

        }
        // Fecha arquivo
        file_train.close();
    } else {
        cout << "Problemas ao abrir o aquivo de treino!" << endl;
        exit(1);
    }
    cout << endl;

    cout << "Treinamento finalizado!" << endl;
    std::cout << "Tempo: " << float( clock () - begin_time_train ) /  CLOCKS_PER_SEC << " segundos." <<endl;
    cout << "Número de exemplos de treino: " << num_train-1 << endl;
    cout << endl;




    /** Test **/
    int num_test = 0;
    const clock_t begin_time_test = clock();
    Matriz confusion = Matriz(n_saida,n_saida);

    // Abre aquivo
    ifstream file_test (argv[2]);
    if (file_test.is_open()) {
        // Varre aquivo
        while (!file_test.eof()) {
            getline (file_test,line);
            istringstream iss(line);

            // Varre linhas
            int coluna = 0;
            while(iss >> element) {
                // Armazena cada elemento das linhas em um vetor
                double value = atof(element.c_str());
                entrada.set(0, coluna, value);
                coluna++;
            }

            if(entrada.get(0,0) >= n_saida) continue;

            num_test++;
            double t = entrada.get(0,0); // target
            double out = rna.test(entrada.reduzColuna(0));

            for (int i = 0; i < n_saida; i++) {
                for (int j = 0; j < n_saida; j++) {
                   if(t == i && out == j) confusion.set(i,j, confusion.get(i,j)+1);
                }
            }
        }
        // Fecha arquivo
        file_test.close();
    } else {
        cout << "Problemas ao abrir o aquivo de treino!" << endl;
        exit(1);
    }
    cout << endl;

    cout << "Teste finalizado!" << endl;
    std::cout << "Tempo: " << float( clock () - begin_time_test ) /  CLOCKS_PER_SEC << " segundos." <<endl;
    cout << "Número de exemplos de teste: " << num_test-1 << endl;
    cout << "Matriz de confusão:" << endl;
    cout << confusion.toString() << endl;

    double TP = 0;
    double total = 0;
    for (int i = 0; i < n_saida; i++) {
        for (int j = 0; j < n_saida; j++) {
           if(i == j) TP += confusion.get(i,j);
           total += confusion.get(i,j);
        }
    }
    cout << "Acurácia: "<< TP/total * 100 << "%" << endl;

    return 0;
}


