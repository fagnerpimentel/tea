#include <iostream>
#include <map>

#include "include/Util.hpp"

using namespace std;

int main(int argc, char *argv[])
{

    /** Leitura dos dados do arquivo **/
    vector<vector<string>> dados = readCSV(argv[1]);

    /** separação dos dados **/
    cout << "Dados:" << endl;
    vector<string> names;
    vector<vector<string>> X;
    vector<string> Y;
    for (unsigned int linha = 0; linha < dados.size(); linha++) {
        vector<string> v;
        for (unsigned int coluna = 1; coluna < dados.at(linha).size(); coluna++) {
            cout << dados.at(linha).at(coluna) << " ";
            if(linha == 0)
                names.push_back(dados.at(linha).at(coluna));
            else if(coluna == dados.at(linha).size()-1)
                Y.push_back(dados.at(linha).at(coluna));
            else if(coluna != dados.at(linha).size()-1)
                v.push_back(dados.at(linha).at(coluna));
        }
        if(linha != 0) X.push_back(v);
        cout << endl;
    }

    /** contagem de classes **/
    map<string, double> P;
    for (unsigned int linha = 0; linha < Y.size(); linha++) {
        if(P.find(Y.at(linha)) == P.end()) P.insert(pair<string,int>(Y.at(linha), 1));
        else P.find(Y.at(linha))->second++;
    }
    int classes = P.size();

    /** contagem das caracteristicas **/
    vector<map<string,Matriz>> probabilidades;
    for (unsigned int coluna = 0; coluna < X.at(0).size(); coluna++) {
        map<string, Matriz> m;
        for (unsigned int linha = 0; linha < X.size(); linha++) {

            map<string, double>::iterator it;
            int k = 0;
            for (it = P.begin(); it != P.end(); it++) {
//            for (int k = 0; k < classes; k++) {
                if(m.find(X.at(linha).at(coluna)) == m.end()){
                    m.insert(pair<string,Matriz>(X.at(linha).at(coluna), Matriz(1,classes)));
                }
                if(Y.at(linha) == it->first){
                    m.find(X.at(linha).at(coluna))->second.set(0,k,
                        m.find(X.at(linha).at(coluna))->second.get(0,k)+1);
                }
                k++;
            }
        }
        probabilidades.push_back(m);
    }

    /** Criação da look up table **/

    cout << endl;
    cout << "LookUPTable:" << endl;
    cout << endl;

    map<string, double>::iterator it;
    for (it = P.begin(); it != P.end(); it++) {
        cout << it->first << " ";
    }
    cout << endl;
    cout << endl;

    vector<map<string, Matriz>>::iterator it_v;
    int m = 0;
    for (it_v = probabilidades.begin(); it_v != probabilidades.end(); it_v++) {
        map<string, Matriz>::iterator it_m;
        cout << names.at(m) << ":" << endl ;
        for (it_m = it_v->begin(); it_m != it_v->end(); it_m++) {
            map<string, double>::iterator it;
            int k = 0;
            for (it = P.begin(); it != P.end(); it++) {
//            for (int k = 0; k < classes; k++) {
                cout << it_m->second.get(0,k) << "/" << P.find(it->first)->second << " ";
                k++;
            }

            cout << ":" << it_m->first << endl ;

        }
        cout << endl;
        m++;
    }

    for (it = P.begin(); it != P.end(); it++) {
        cout << it->second << "/" << Y.size() << " ";
    }
    cout << endl;



    /** Classificação **/
    vector<string> nova_entrada;// = {0, 0, 2, 1};
    while(true){
        cout << "############" << endl;
        cout << endl;

        // espera entrada do usuário
        nova_entrada.clear();
        for (unsigned int i = 0; i < probabilidades.size(); i++) {
            string in = "";
            while(probabilidades.at(i).find(in) == probabilidades.at(i).end()){
                cout << "Entre com parâmetro '" << names.at(i) << "':" << endl;
                cin >> in;
            }
            nova_entrada.push_back(in);

        }
        cout << endl;

        vector<double> classificacao;
        map<string, double>::iterator ind_class = P.begin();
        double big = 0;
        map<string, double>::iterator it;
        int k = 0;
        for (it = P.begin(); it != P.end(); it++) {
//        for (int k = 0; k < classes; k++) {
            double value = 1.0;
            for (unsigned int i = 0; i < probabilidades.size(); i++) {
                cout << probabilidades.at(i).find(nova_entrada.at(i))->second.get(0,k) << "/" << it->second << " * ";
                value *= probabilidades.at(i).find(nova_entrada.at(i))->second.get(0,k) / it->second;
            }
            value *= it->second/Y.size();
            cout << it->second << "/" << Y.size() << " = " << value << endl;
            classificacao.push_back(value);
            if(value > big){
                big = value;
                ind_class = it;
            }
            k++;
        }

        cout << endl;
        cout << "Classe: " << ind_class->first << endl;
        cout << endl;

    }

    return 0;
}

