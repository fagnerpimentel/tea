#include "../include/rna.hpp"

#define RANDON_MAX 0.1

RNA::RNA(int n, int m, int h, int H, int max_it, double e, double eta){
    this->n = n;
    this->m = m;
    this->h = h;
    this->H = H;

    this->max_it = max_it;

    this->e = e;
    this->eta = eta;

    Mersenne mersenne;

    // inicia pesos: e -> h
    Matriz wh = Matriz(this->n,this->h);
    for (int linha = 0; linha < wh.linhas(); linha++) {
        for (int coluna = 0; coluna < wh.colunas(); coluna++) {
            wh.set(linha, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
        }
    }
    this->wh.push_back(wh);
    // inicia pesos: bias -> h
    Matriz whb = Matriz(1,this->h);
    for (int coluna = 0; coluna < whb.colunas(); coluna++) {
        whb.set(0, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
    }
    this->whb.push_back(whb);

    for (int i = 0; i < this->H; i++) {
        // inicia pesos: h -> h
        Matriz wh = Matriz(this->h,this->h);
        for (int linha = 0; linha < wh.linhas(); linha++) {
            for (int coluna = 0; coluna < wh.colunas(); coluna++) {
                wh.set(linha, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
            }
        }
        this->wh.push_back(wh);
        // inicia pesos: bias -> h
        Matriz whb = Matriz(1,this->h);
        for (int coluna = 0; coluna < whb.colunas(); coluna++) {
            whb.set(0, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
        }
        this->whb.push_back(whb);
    }

    // inicia pesos: h -> s
    this->ws = Matriz(this->h,this->m);
    for (int linha = 0; linha < this->ws.linhas(); linha++) {
        for (int coluna = 0; coluna < this->ws.colunas(); coluna++) {
            this->ws.set(linha, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
        }
    }
    // inicia pesos: bias -> s
    this->wsb = Matriz(1,this->m);
    for (int coluna = 0; coluna < this->wsb.colunas(); coluna++) {
        this->wsb.set(0, coluna, mersenne.myRandom(-RANDON_MAX,RANDON_MAX));
    }
}

string
RNA::info(){
    ostringstream s;
    s << "Número de entradas: " << n << endl;
    s << "Número de processadores na saída: " << m << endl;
    s << "Número de processadores em cada camada escondida: " << h << endl;
    s << "Número de camadas escondidas: " << H << endl;
    s << "Taxa de aprendizado: " << eta << endl;
    s << "Erro mínimo: " << e << endl;
    s << "Máximo de iterações: " << max_it << endl;
    return s.str();
}

double
RNA::f_net(double value){
    return 1/(1 + exp(-value));
}
double
RNA::f_linha_net(double f){
    return f * (1 - f);
}

void
RNA::train(Matriz entrada){

    double t = entrada.get(0,0); // target
    entrada = entrada.reduzColuna(0); // X

    // binary target
    Matriz target = Matriz(1, this->m);
    for (int coluna = 0; coluna < this->m; coluna++) {
        target.set(0,coluna,(t == coluna)?1:0);
    }

    // fnet dos neorônios
    vector<Matriz> fh;
    for (int i = 0; i < this->H; i++) {
        fh.push_back(Matriz(1,this->h));
    }
    Matriz fs = Matriz(1,this->m);

    // erro dos neorônios
    Matriz e = Matriz(1,this->m);
    Matriz es = Matriz(1,this->m);
    vector<Matriz> eh;
    for (int i = 0; i < this->H; i++) {
        eh.push_back(Matriz(1,this->h));
    }

    int cont;
    for (cont = 0; cont < this->max_it; cont++) {
        int h_begin = 0;
        int h_end = this->H - 1;

        // forward e -> h
        for (int i = 0; i < this->h; i++) {
            double soma = 0;
            for (int j = 0; j < this->n; j++) {
                soma += entrada.get(0, j)*this->wh.at(h_begin).get(j,i);
            }
            soma += this->whb.at(h_begin).get(0,i) * (-1);
            fh.at(h_begin).set(0,i, f_net(soma));
        }

        // forward h -> h
        for (int k = 0; k < this->H-1; k++) {
            for (int i = 0; i < this->h; i++) {
                double soma = 0;
                for (int j = 0; j < this->h; j++) {
                    soma += fh.at(k).get(0, j)*this->wh.at(k+1).get(j,i);
                }
                soma += this->whb.at(k+1).get(0,i) * (-1);
                fh.at(k+1).set(0,i, f_net(soma));
            }
        }

        // forward h -> s
        for (int i = 0; i < this->m; i++) {
            double soma = 0;
            for (int j = 0; j < this->h; j++) {
                soma += fh.at(h_end).get(0, j)*this->ws.get(j,i);
            }
            soma += this->wsb.get(0,i) * (-1);
            fs.set(0,i, f_net(soma));
        }

        // erro na saida
        bool conv = true;
        for (int i = 0; i < this->m; i++) {
            double erro = target.get(0,i) - fs.get(0,i);
            e.set(0,i, erro);
            if(fabs(erro) > this->e) conv = false;
        }

//        cout << "#####" << endl;
//        cout << target.toString() << endl;
//        cout << fs.toString() << endl;
//        cout << e.toString() << endl;
//        getchar();
        if(conv) break;

        // erro propagado da saida
        for (int i = 0; i < this->m; i++) {
            es.set(0,i, e.get(0,i) * f_linha_net(fs.get(0,i)));
        }

        // backward s -> h
        for (int i = 0; i < this->h; i++) {
            for (int j = 0; j < this->m; j++) {
                ws.set(i,j, ws.get(i,j) + this->eta * es.get(0,j) * fh.at(h_end).get(0,i));
                wsb.set(0,j, wsb.get(0,j) + this->eta * es.get(0,j) * (-1));
            }
        }
        for (int i = 0; i < this->h; i++) {
            double soma_pesos = 0;
            for (int j = 0; j < this->m; j++) {
                soma_pesos += es.get(0,j) * ws.get(i,j);                }
            double erro = soma_pesos * f_linha_net(fh.at(h_end).get(0,i));

            eh.at(h_end).set(0,i, erro);
        }

        // backward h -> h
        for (int k = this->H-1; k > 0 ; k--) {
            for (int i = 0; i < this->h; i++) {
                for (int j = 0; j < this->h; j++) {
                    wh.at(k).set(i,j, wh.at(k).get(i,j) + this->eta * eh.at(k).get(0,j) * fh.at(k-1).get(0,i));
                    whb.at(k).set(0,j, whb.at(k).get(0,j) + this->eta * eh.at(k).get(0,j) * (-1));
                }
            }

            for (int i = 0; i < this->h; i++) {
                double soma_pesos = 0;
                for (int j = 0; j < this->h; j++) {
                    soma_pesos += eh.at(k).get(0,j) * wh.at(k).get(i,j);                }
                double erro = soma_pesos * f_linha_net(fh.at(k-1).get(0,i));

                eh.at(k-1).set(0,i, erro);
            }
        }

        // backward h -> e
        for (int i = 0; i < this->n; i++) {
            for (int j = 0; j < this->h; j++) {
                wh.at(h_begin).set(i,j, wh.at(h_begin).get(i,j) + this->eta * eh.at(h_begin).get(0,j) * entrada.get(0,i));
                whb.at(h_begin).set(0,j, whb.at(h_begin).get(0,j) + this->eta * eh.at(h_begin).get(0,j) * (-1));
            }
        }
    }
}

double
RNA::test(Matriz entrada){

    int h_begin = 0;
    int h_end = this->H - 1;

    // fnet dos neorônios
    vector<Matriz> fh;
    for (int i = 0; i < this->H; i++) {
        fh.push_back(Matriz(1,this->h));
    }
    Matriz fs = Matriz(1,this->m);

    // forward e -> h
    for (int i = 0; i < this->h; i++) {
        double soma = 0;
        for (int j = 0; j < this->n; j++) {
            soma += entrada.get(0, j)*this->wh.at(h_begin).get(j,i);
        }
        soma += this->whb.at(h_begin).get(0,i) * (-1);
        fh.at(h_begin).set(0,i, f_net(soma));
    }

    // forward h -> h
    for (int k = 0; k < this->H-1; k++) {
        for (int i = 0; i < this->h; i++) {
            double soma = 0;
            for (int j = 0; j < this->h; j++) {
                soma += fh.at(k).get(0, j)*this->wh.at(k+1).get(j,i);
            }
            soma += this->whb.at(k+1).get(0,i) * (-1);
            fh.at(k+1).set(0,i, f_net(soma));
        }
    }

    // forward h -> s
    for (int i = 0; i < this->m; i++) {
        double soma = 0;
        for (int j = 0; j < this->h; j++) {
            soma += fh.at(h_end).get(0, j)*this->ws.get(j,i);
        }
        soma += this->wsb.get(0,i) * (-1);
        fs.set(0,i,f_net(soma));
    }

    int ind = 0;
    double bigValue = fs.get(0,0);
    for (int i = 1; i < this->m; ++i) {
        if(fs.get(0,i) > bigValue) {
            bigValue = fs.get(0,i);
            ind = i;
        }
    }
//    cout << "#####" << endl;
//    cout << entrada.toString() << endl;
//    cout << fs.toString() << endl;
//    getchar();

    return ind;
}
