#include "../include/Matriz.hpp"

Matriz::Matriz(int linhas, int colunas, double value) {

    if(linhas < 0 || colunas < 0){
        cout << "Não é possível setar valor negativo para as dimensões da matriz." << endl;
        exit(1);
    }

    mLinhas = linhas;
    mColunas = colunas;

    vector<double> valores;
    for (int linha = 0; linha < mLinhas; linha++) {
        valores.clear();
        for (int coluna = 0; coluna < mColunas; ++coluna) {
            valores.push_back(value);
        }
        mMatriz.push_back(valores);
    }
}

int
Matriz::linhas(){
    return mLinhas;
}

int
Matriz::colunas(){
    return mColunas;
}

double
Matriz::get(int linha, int coluna){
    return mMatriz.at(linha).at(coluna);
}

void
Matriz::set(int linha, int coluna, double valor){
    mMatriz.at(linha).at(coluna) = valor;
}

int
Matriz::ordem(){
    if(this->linhas() != this->colunas()){
        cout << "Não é possivel determinar a ordem de uma matriz não quadrada." << endl;
        exit(1);
    }

    return this->linhas();
}

double
Matriz::determinante(){

    int ordem = this->ordem();

    double det = 0;

    switch (ordem) {
    case 0:
        cout << "Não é possivel encontrar o determinante de uma matriz nula." << endl;
        exit(1);
        break;
    case 1:
        det = this->get(0,0);
        break;
    case 2:
        det = this->det_2x2();
        break;
    case 3:
        det = this->det_3x3();
        break;
    default:
        det = this->det_nxn();
        break;
    }
    return det;
}

double
Matriz::det_2x2(){
    double det = (this->get(0,0) * this->get(1,1))
              - (this->get(0,1) * this->get(1,0));
    return det;
}

double
Matriz::det_3x3(){
    double p1 = (this->get(0,0) * this->get(1,1) * this->get(2,2));
    double p2 = (this->get(0,1) * this->get(1,2) * this->get(2,0));
    double p3 = (this->get(0,2) * this->get(1,0) * this->get(2,1));
    double s1 = (this->get(0,2) * this->get(1,1) * this->get(2,0));
    double s2 = (this->get(0,0) * this->get(1,2) * this->get(2,1));
    double s3 = (this->get(0,1) * this->get(1,0) * this->get(2,2));

    double det = p1 + p2 + p3 - s1 - s2 - s3;
    return det;
}

double
Matriz::det_nxn(){
    double det = 0;
    for (int linha = 0; linha < this->linhas(); linha++) {
        det += pow(-1, linha) * get(linha, 0) * reducao(linha, 0).determinante();
    }
    return det;
}


Matriz
Matriz::transposta(){
    Matriz T = Matriz(this->colunas(), this->linhas());
    for (int linha = 0; linha < T.linhas(); ++linha) {
        for (int coluna = 0; coluna < T.colunas(); ++coluna) {
            T.set(linha, coluna, get(coluna, linha));
        }
    }
    return T;
}

Matriz
Matriz::cofatores(){
    /** O cofator de M_{l,c} pode ser definida por:
     **   (-1)^{l+c} * det(M_{l,c}) onde:
     **     det(M_{l,c}) = determinante da matriz reduzida de M na linha l e coluna c;
     **/

    Matriz cofatores = Matriz(this->linhas(), this->colunas());
    for (int linha = 0; linha < cofatores.linhas(); linha++) {
        for (int coluna = 0; coluna < cofatores.colunas(); coluna++) {

            // Cofator de M_{l,c}: (-1)^(l+c) * det(M_{l,c})
            double cofator = pow(-1,(linha+1)+(coluna+1)) * this->reducao(linha, coluna).determinante();

            cofatores.set(linha, coluna, cofator);
        }
    }
    return cofatores;
}

Matriz
Matriz::adjunta(){
    /** adj(M) pode ser definida por:
     **   cof(M)' onde:
     **     cof(M) = matriz de cofatores de M
     **     ' representa transposta
     **/

    Matriz adj = this->cofatores().transposta();
    return adj;
}

Matriz
Matriz::inversa(){
    /** A matriz inversa pode ser definida por:
     **   1/det(M)*adj(M) onde:
     **     det(M) = determinante e M;
     **     adj(M) = matriz adjunta de M;
     **/

    // Determinante
    double det = this->determinante();

    if(det == 0){
        cout << "Esta matriz não possui inversa." << endl;
        exit(1);
    }

    // Matriz adjunta
    Matriz adj = this->adjunta();

    // Matriz inversa
    Matriz inversa = adj * (1.0/det);

    return inversa;
}

Matriz
Matriz::media(){
    Matriz media = Matriz(1, this->colunas());
    for (int linha = 0; linha < this->linhas(); linha++) {
        for (int coluna = 0; coluna < this->colunas(); coluna++) {
            media.set(0,coluna, media.get(0, coluna) + this->get(linha,coluna));
        }
    }
    for (int coluna = 0; coluna < this->colunas(); ++coluna) {
        media.set(0, coluna, media.get(0, coluna) / this->linhas());
    }

    return media;
}

Matriz
Matriz::covariancia(){
    Matriz covariancia = Matriz(this->colunas(), this->colunas());
    for (int x = 0; x < this->colunas(); x++) {
        for (int y = 0; y < this->colunas(); y++) {
            for (int i = 0; i < this->linhas(); i++) {
                covariancia.set(x,y, covariancia.get(x,y) +
                         (this->get(i, x)) *
                         (this->get(i, y)));
            }
        }
    }
    for (int x = 0; x < this->colunas(); x++) {
        for (int y = 0; y < this->colunas(); y++) {
            covariancia.set(x,y, covariancia.get(x,y) / (this->linhas()-1));
        }
    }

    return covariancia;
}

Matriz
Matriz::reducao(int rLinha, int rColuna){
    Matriz reducao =  Matriz(this->linhas()-1, this->colunas()-1);

    int l = 0;
    for (int linha = 0; linha < this->linhas(); linha++) {
        int c = 0;
        for (int coluna = 0; coluna < this->colunas(); coluna++) {

            if(linha == rLinha) {
                l = 1;
                continue;
            }
            if(coluna == rColuna) {
                c = 1;
                continue;
            }

            reducao.set(linha-l,coluna-c, this->get(linha, coluna));
        }
    }

    return reducao;
}

void
Matriz::eigen(int iteracoes, double tolerancia, Matriz& autoValores, Matriz& autoVetores){

    // inicia auto vetores com identidade
    autoValores = *this;
    for (int o = 0; o < autoVetores.ordem(); o++) {
        autoVetores.set(o,o, 1);
    }

    // realiza iterações
    int k = 0;
    while (k < iteracoes) {
        k++;

        // encontra maior valor em modulo fora da diagonal
        int indiceLinha = 0;
        int indiceColuna = 0;
        double maiorValorEmModuloForaDiagonal = 0.0;
        for (int linha = 0; linha < autoValores.linhas(); linha++) {
            for (int coluna = 0; coluna < autoValores.colunas(); coluna++) {
                if(linha == coluna) continue;

                if(fabs(autoValores.get(linha, coluna)) > maiorValorEmModuloForaDiagonal) {
                    maiorValorEmModuloForaDiagonal = fabs(autoValores.get(linha, coluna));
                    indiceLinha = linha;
                    indiceColuna = coluna;
                }
            }
        }

        // finaliza caso valor seja menor que a tolerância
        if(maiorValorEmModuloForaDiagonal < tolerancia){
            return;
        }

        // calcula phi
        double phi = (autoValores.get(indiceColuna, indiceColuna) -
               autoValores.get(indiceLinha, indiceLinha)) /
           (2 * autoValores.get(indiceLinha, indiceColuna));

        // calcula valores de rotação
        int sinal = (phi >= 0) ? 1 : -1;
        double t = (phi == 0) ? 1 : 1/(phi + sinal*sqrt(pow(phi,2)+1));
        double c = 1/(sqrt(1+pow(t,2)));
        double s = t/(sqrt(1+pow(t,2)));

        // constroi matriz de rotação
        Matriz U = Matriz(autoValores.linhas(), autoValores.colunas());
        for (int o = 0; o < autoValores.ordem(); o++) {
            U.set(o,o, 1);
        }
        U.set(indiceLinha, indiceLinha, c);
        U.set(indiceColuna, indiceColuna, c);
        U.set(indiceLinha, indiceColuna, s);
        U.set(indiceColuna, indiceLinha, -s);

        // determina auto valores e auto vetores
        autoValores = U.transposta() * autoValores * U;
        autoVetores =  autoVetores * U;
    }

    // matriz não converge
    cout << "Máximo de iterações alcançado." << endl;
    exit(1);
}

Matriz
Matriz::operator*(Matriz M){

    if(this->colunas() != M.linhas()){
        cout<< "Não é possível multiplicar por essa matriz." << endl;
        exit(1);
    }

    int ordem = M.linhas();

    Matriz resultado = Matriz(this->linhas(), M.colunas());

    for(int linha = 0; linha < resultado.linhas(); linha++) {
        for(int coluna = 0; coluna < resultado.colunas(); coluna++) {
            for(int k = 0; k < ordem; k++) {
                resultado.set(linha,coluna,
                             resultado.get(linha,coluna) +
                             (this->get(linha,k)*M.get(k,coluna)));
            }
        }
    }

    return resultado;
}

Matriz
Matriz::operator*(double d){
    Matriz resultado = Matriz(this->linhas(), this->colunas());

    for(int linha = 0; linha < resultado.linhas(); linha++) {
        for(int coluna = 0; coluna < resultado.colunas(); coluna++) {
            resultado.set(linha,coluna, this->get(linha, coluna) * d);
        }
    }

    return resultado;
}

Matriz
Matriz::operator+(Matriz M){

    if(this->linhas() != M.linhas() || this->colunas() != M.colunas()){
        cout<< "Não é possível somar estas matrizes." << endl;
        exit(1);
    }

    Matriz resultado = Matriz(this->linhas(), this->colunas());

    for (int linha = 0; linha < resultado.linhas(); linha++) {
        for (int coluna = 0; coluna < resultado.colunas(); coluna++) {
            resultado.set(linha, coluna, this->get(linha,coluna) + M.get(linha, coluna));
        }
    }

    return resultado;
}

Matriz
Matriz::operator-(Matriz M){
    if(this->linhas() != M.linhas() || this->colunas() != M.colunas()){
        cout<< "Não é possível subtrair estas matrizes." << endl;
        exit(1);
    }

    Matriz resultado = Matriz(this->linhas(), this->colunas());

    for (int linha = 0; linha < resultado.linhas(); linha++) {
        for (int coluna = 0; coluna < resultado.colunas(); coluna++) {
            resultado.set(linha, coluna, this->get(linha,coluna) - M.get(linha, coluna));
        }
    }

    return resultado;
}

Matriz
Matriz::getLinha(int linha){
    if(linha >= this->linhas())
        throw std::out_of_range( "Matriz::getLinha(int linha) - Linha inexistente" );

    Matriz mlinha = Matriz(1, this->colunas());
    for (int coluna = 0; coluna < this->colunas(); coluna++) {
        mlinha.set(0, coluna, this->get(linha, coluna));
    }
    return mlinha;
}

Matriz
Matriz::getColuna(int coluna){
    if(coluna >= this->colunas())
        throw std::out_of_range( "Matriz::getColuna(int coluna) - Coluna inexistente" );

    Matriz mColuna = Matriz(this->linhas(), 1);
    for (int linha = 0; linha < this->linhas(); linha++) {
        mColuna.set(linha, 0, this->get(linha, coluna));
    }
    return mColuna;
}

pair<int, int>
Matriz::getMax(){
    double max = this->get(0,0);
    pair<int, int> index(0,0);
    for (int linha = 0; linha < this->linhas(); linha++) {
        for (int coluna = 0; coluna < this->colunas(); coluna++) {
            if(this->get(linha, coluna) > max){
                max = this->get(linha, coluna);
                index.first = linha;
                index.second = coluna;
            }
        }
    }
    return index;
}

pair<int, int>
Matriz::getMin(){
    double min = this->get(0,0);
    pair<int, int> index(0,0);
    for (int linha = 0; linha < this->linhas(); linha++) {
        for (int coluna = 0; coluna < this->colunas(); coluna++) {
            if(this->get(linha, coluna) < min){
                min = this->get(linha, coluna);
                index.first = linha;
                index.second = coluna;
            }
        }
    }
    return index;
}

Matriz
Matriz::reduzLinha(int rLinha){
    Matriz r = Matriz(this->linhas(), this->colunas()-1);

    int l = 0;
    for (int linha = 0; linha < this->linhas(); linha++) {
        for (int coluna = 0; coluna < this->colunas(); coluna++) {
            if(linha == rLinha) {
                l = 1;
                continue;
            }
            r.set(linha-l,coluna, this->get(linha, coluna));
        }
    }
    return r;
}

Matriz
Matriz::reduzColuna(int rColuna){
    Matriz r = Matriz(this->linhas(), this->colunas()-1);

    for (int linha = 0; linha < this->linhas(); linha++) {
        int c = 0;
        for (int coluna = 0; coluna < this->colunas(); coluna++) {
            if(coluna == rColuna) {
                c = 1;
                continue;
            }
            r.set(linha,coluna-c, this->get(linha, coluna));
        }
    }
    return r;
}

string
Matriz::toString(){
    ostringstream ss;
    for (int linha = 0; linha < this->linhas(); linha++ ) {
        for (int coluna = 0; coluna < this->colunas(); coluna++ ) {
            ss << get(linha,coluna) << " ";
        }
        ss << endl;
    }
    return ss.str();
}
