#include <iostream>
#include <fstream>
#include <unistd.h>

#include "include/Matriz.hpp"
#include "include/Util.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    /** Leitura dos dados do arquivo **/
    Matriz dados = readFile(argv[1]);
    cout << "Dados do arquivo:" << endl;
    cout << dados.toString() << endl;

    /** Seleçao do tipo **/

    cout << "Escolha o tipo de método:" << endl;
    cout << "1 - linear." << endl;
    cout << "2 - quadratico." << endl << endl;
    int tipo = 0;
    cin >> tipo;

    // Quantidade de linhas
    int m = dados.linhas();
    // Quantidades de colunas na matrix X dependendo do tipo do método.
    int n = 0;

    cout << "Tipo escolhido: ";
    switch (tipo) {
    case 1:
        cout << "Linear." << endl << endl;
        n = 2;
        break;
    case 2:
        cout << "Quadratico." << endl << endl;
        n = 3;
        break;
    default:
        cout << "Opção não disponível" << endl << endl;
        exit(1);
        break;
    }

    // pausa para visualização do tipo selecionado
    sleep(2);

    /** Implementação do método **/

    // Matriz X
    cout << "Matriz X:" << endl;
    Matriz X = Matriz(m,n);
    for (int i = 0; i < m; i++ ) {
        X.set(i,0,1); // bias
        X.set(i,1,dados.get(i, 0)); // x
        if(tipo == 2) X.set(i, 2, pow(dados.get(i, 0),2)); // x²
    }
    cout << X.toString() << endl;

    // Matriz Y
    cout << "Matriz Y:" << endl;
    Matriz Y = Matriz(m,1);
    for (int i = 0; i < m; i++ ) {
        Y.set(i,0,dados.get(i, 1));
    }
    cout << Y.toString() << endl;

    // Matriz X'
    cout << "Matriz Xt (X transposta):" << endl;
    Matriz Xt = X.transposta();
    cout << Xt.toString() << endl;

    // Matriz XX = X'*X
    cout << "Matriz XX = X'*X:" << endl;
    Matriz XX = Xt * X;
    cout << XX.toString() << endl;

    // Matriz XY = X'*Y
    cout << "Matriz XY = X'*Y:" << endl;
    Matriz XY = Xt * Y;
    cout << XY.toString() << endl;

    // Matriz XX^{-1}
    cout << "Matriz XX^{-1}:" << endl;
    Matriz XXinv = XX.inversa();
    cout << XXinv.toString() << endl;

    // Matriz B = XX^{-1}*XY
    cout << "Matriz B = XX^{-1}*XY:" << endl;
    Matriz B = XXinv * XY;
    cout << B.toString() << endl;

    // pausa para visualização do resultado
    sleep(2);

    /** Definição da equação de regreção **/

    double a,b,c;
    a = B.get(0,0);
    b = B.get(1,0);
    if(tipo == 2) c = B.get(2,0);

    cout << "Equação de regreção encontrada:" << endl;
    cout << "y = (" << a << ") + ("<< b << ") * x " ;
    if(tipo == 2) cout << " + (" << c << ") * x^2" ;
    cout << endl << endl;

    /** Estimativa de novos valores **/

    while(true){
        cout << "Entre com um novo valor: " ;
        double novoValor = 0;
        cin >> novoValor;
        double resultado = 0;
        resultado += a;
        resultado += b*novoValor;
        if(tipo == 2)resultado += c*novoValor*novoValor;
        cout << "Resultado: " << resultado << endl << endl;
    }

    return 0;
}


